
const movieList = [
  {
    title: "Avatar",
    releaseYear: 2009,
    duration: 162,
    director: "James Cameron",
    actors: [
      "Sam Worthington",
      "Zoe Saldana",
      "Sigourney Weaver",
      "Stephen Lang",
    ],
    description:
      "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
    rating: 7.9,
  },
  {
    title: "300",
    releaseYear: 2006,
    duration: 117,
    director: "Zack Snyder",
    actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
    description:
      "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
    rating: 7.7,
  },
  {
    title: "The Avengers",
    releaseYear: 2012,
    duration: 143,
    director: "Joss Whedon",
    actors: [
      "Robert Downey Jr.",
      "Chris Evans",
      "Mark Ruffalo",
      "Chris Hemsworth",
    ],
    description:
      "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    rating: 8.1,
  },

  {
   title: "Back to the Future",
   releaseYear: 1985,
   duration: 116,
   director: "Robert Zemeckis",
   actors: [
    "Michael J.Fox",
    "Christopher LIoyd",
    "Crispin Glover",
   ],
   description: "Marty McFly, a 17-year-old high school student, is accidentally sent 30 years into the past in a time-traveling DeLorean invented by his close friend, the maverick scientist Doc Brown.",
   poster: "https://image.tmdb.org/t/p/w300_and_h450_bestv2/hQq8xZe5uLjFzSBt4LanNP7SQjl.jpg",
   rating: 8.5,
  },
];

//déclaration object(nouveau film)
const newMovie = {
  title: "Lord of the rings",
  releaseYear: 2001,
  duration: 178,
  director: "Peter Jackson",
  actors: ["Elijah Wood", "Ian Mckellen"],
  description:
    "Un Hobbit du Comté et ses huit compagnons se mettent en route pour détruire le puissant anneau unique et sauver la Terre du Milieu du terrible Sauron.",
  poster:
    "https://image.tmdb.org/t/p/w600_and_h900_bestv2/5OPg6M0yHr21Ovs1fni2H1xpKuF.jpg",
  rating: 8,
};

//fonction pour ajouter un film
movieList.push(newMovie);

//affiche la nouvelle liste des films
console.log(movieList);

//fonction pour supprimer un film
movieList.shift(newMovie);

//on récupère l'élèment DOM
const movieListElement = document.getElementById("movieList");

//boucle qui parcours la liste des films
movieList.forEach((movie, index) => {
  //on fait un élèment DIV pour chaque film
  const movieElement = document.createElement("div");

  //ajouter une classe unique à chaque div
  movieElement.classList.add(`movie-${index}`);

  //on ajoute le contenu hTML au DIV (élèment pour chaque film)
  movieElement.innerHTML = `<h2>${movie.title}</h2>
                            <img src=${movie.poster}/>
                            <p>duration: ${movie.duration}</p>
                            <p>releaseYear: ${movie.releaseYear}</p>
                            <p>director: ${movie.director}</p>
                            <p>actors: ${movie.actors}</p>
                            <p>description: ${movie.description}</p>                            
                            <p>rating: ${movie.rating}</p>`;

  // Sélectionner l'élément img et ajouter une classe unique
  const imgElement = movieElement.querySelector("img");
  imgElement.classList.add(`poster-${index}`);

  const titleElement = movieElement.querySelector("h2");
  titleElement.classList.add(`title-${index}`);

  //on ajoute le contenu film à la liste film
  movieListElement.appendChild(movieElement);
});
